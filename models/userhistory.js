"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.UserGame, {
        foreignKey: "user_game_id",
        targetKey: "id",
        onDelete: "CASCADE",
      });
    }
  }
  UserHistory.init(
    {
      game_name: { type: DataTypes.STRING, allowNull: false },
      score: { type: DataTypes.INTEGER, allowNull: false },
      play_time: { type: DataTypes.INTEGER, allowNull: false },
      user_game_id: { type: DataTypes.INTEGER, allowNull: false },
    },
    {
      sequelize,
      modelName: "UserHistory",
    }
  );
  return UserHistory;
};
