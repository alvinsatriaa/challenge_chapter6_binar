"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "UserBiodata",
      [
        {
          name: "Name 1",
          company: "Binar Academy",
          description: "bla bla bla",
          user_game_id: 1,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          name: "Name 2",
          company: "Binar Academy",
          description: "bla bla bla",
          user_game_id: 2,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          name: "Name 3",
          company: "Binar Academy",
          description: "bla bla bla",
          user_game_id: 3,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
