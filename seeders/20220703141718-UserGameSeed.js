"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "UserGames",
      [
        {
          // id: 3,
          username: "username-1",
          password: "123456",
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 3,
          username: "username-2",
          password: "123456",
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 3,
          username: "username-3",
          password: "123456",
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    const Op = Sequelize.Op;
    queryInterface.bulkDelete(
      "UserGames",
      {
        username: {
          [Op.in]: ["username-3"],
        },
      },
      {}
    );
  },
};
