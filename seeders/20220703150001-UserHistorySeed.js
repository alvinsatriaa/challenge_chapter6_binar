"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "UserHistories",
      [
        {
          game_name: "Fifa",
          score: 23,
          play_time: 30,
          user_game_id: 1,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          game_name: "Mobile Legend",
          score: 33,
          play_time: 60,
          user_game_id: 2,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          game_name: "FF",
          score: 50,
          play_time: 60,
          user_game_id: 3,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
