const express = require("express");
const app = express();
const port = 3000;
const fs = require("fs");
let users = require("./data_login.json");
let data = require("./data_user.json");
const { UserGame, UserBiodata, UserHistory } = require("./models");

app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("node_modules"));
app.use(express.static("views"));

app.get("/", (request, response) => {
  response.render("index", {
    name: users[0].username,
    data: data,
  });
});

app.get("/play", (request, response) => {
  response.render("index2", {
    title: "ROCK PAPER SCISSORS",
    displayId: true,
    viewOnly: false,
  });
});

app.post("/login", (request, response) => {
  // terima data yg dikirim oleh FE
  const { username, password } = request.body;
  //   console.log({ username, password });
  //   cek apakah data yg kita terima dari FE, ada ga sama data yg ada di BE
  const user = users.find((item) => item.username === username);
  if (user) {
    if (user.password === password) {
      response.status(200).json({ status: "logged-in", ...user });
    } else {
      response.status(403).json({ status: "invalid-password" });
    }
  } else {
    response.status(404).json({ status: "no-username" });
  }
});

app.get("/userDashboard", async (req, res) => {
  const result = await UserGame.findAll();
  res.status(200).render("index3", { result });
});

app.get("/userDetail/:id", async (req, res) => {
  const detailUserGame = await UserGame.findOne({
    where: { id: +req.params.id },
    include: UserBiodata,
  });
  res.status(200).render("createUser", { detailUserGame });
});

app.get("/userCreate", (req, res) => {
  res.status(200).render("createUser");
});

app.post("/createUserAPI", async (req, res) => {
  const { name, username, password, company, description } = req.body;

  try {
    const userGame = await UserGame.create({ username, password });
    const userBiodata = await UserBiodata.create({
      name,
      company,
      description,
      user_game_id: userGame.id,
    });

    res.status(200).redirect("/userDashboard");
  } catch (error) {
    res.status(500).redirect("/userDashboard", { error });
  }
});

app.get("/userUpdate/:id", async (req, res) => {
  const detailUserGame = await UserGame.findOne({
    where: { id: +req.params.id },
    include: UserBiodata,
  });
  res.status(200).render("userUpdate", { detailUserGame });
});

app.post("/userUpdate/:id", async (req, res) => {
  const { name, username, password, company, description } = req.body;
  await UserGame.update(
    { username, password },
    {
      where: { id: +req.params.id },
    }
  );
  await UserBiodata.update(
    { name, company, description },
    { where: { user_game_id: +req.params.id } }
  );
  res.status(200).redirect("/userDashboard");
});

app.get("/userDelete/:id", async (req, res) => {
  await UserGame.destroy({
    where: { id: +req.params.id },
  });
  res.status(200).redirect("/userDashboard");
});

app.listen(port, () => {
  console.log("Server running on port:", port);
});
