const playerOne = document.getElementById("playerChoice");
const yourName = playerOne.querySelector(".player-name");

const player1 = new HumanPlayer('PLAYER 1');
const player2 = new ComputerPlayer('COM');

yourName.innerText = player1.name;

(async function () {

    console.log("Game start...");

    await player1.choose();
    console.log(`${player1.name} choose: ${player1.choice}`);
  
    await player2.choose();
    console.log(`${player2.name} choose: ${player2.choice}`);
  
    const Rps = new SuitGame(player1, player2);
    console.log(`Match Result: ${Rps.consoleInfo}`);
  
    

})();

