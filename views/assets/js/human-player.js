class HumanPlayer extends Player{

    // choose(){
    //     const option = prompt('Silakan pilih pilihannya: pilih 0 untuk rock, 1 paper, 2 scissor');
    //     this.choice = this.Choices[option];
    // }

    async choose() {
        const human = document.getElementById("playerChoice");
        const playerOpt = human.getElementsByClassName("options");
    
        this.choice = await new Promise((resolve) => {
          for (let i = 0; i < playerOpt.length; i++) {
            const el = playerOpt[i];
            const rock = playerOpt[0];
            const paper = playerOpt[1];
            const scissors = playerOpt[2];
    
            function rockActive() {
              paper.classList.remove("active");
              scissors.classList.remove("active");
              rock.classList.add("active");
              paper.removeEventListener("click", paperActive);
              scissors.removeEventListener("click", scissorsActive);
              rock.removeEventListener("click", rockActive);
              el.removeEventListener("mouseover", hoverBox);
            }
            function paperActive() {
              rock.classList.remove("active");
              scissors.classList.remove("active");
              paper.classList.add("active");
              rock.removeEventListener("click", rockActive);
              scissors.removeEventListener("click", scissorsActive);
              paper.removeEventListener("click", paperActive);
              el.removeEventListener("mouseover", hoverBox);
            }
            function scissorsActive() {
              rock.classList.remove("active");
              paper.classList.remove("active");
              scissors.classList.add("active");
              rock.removeEventListener("click", rockActive);
              paper.removeEventListener("click", paperActive);
              scissors.removeEventListener("click", scissorsActive);
              el.removeEventListener("mouseover", hoverBox);
            }
            function hoverBox() {
              el.style.backgroundColor = "#c4c4c4";
            }
    
            el.addEventListener("click", () => {
              const p1choice = el.dataset.option;
              resolve(p1choice);
            });
            el.addEventListener("mouseover", hoverBox);
            el.addEventListener("mouseleave", () => {
              el.style.backgroundColor = "";
            });
    
            rock.addEventListener("click", rockActive);
            paper.addEventListener("click", paperActive);
            scissors.addEventListener("click", scissorsActive);
          }
        });
      }
}